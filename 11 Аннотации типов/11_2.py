"""
#
# * 11.2 Аннотации типов коллекций
#
# ! Как было до 3.9 мы не рассматриваем
"""


""" Списки """
# Аннотация списка с дополнительным ограничением типа элементов списка
# Аннотируются сразу все элементы
lst: list[int] = [1, 2, 3]  # норм
lst1: list[int] = [1, 2, '3', True]  # ! будет ругаться (mypy, например)



""" Кортежи """
# В кортеже надо использовать аннотацияю каждого элемента
tup1: tuple[str, int] = ('as', 7)
tup2: tuple[str, int] = ('as', '7')  # ! ругань

# Связано с предполагаемым использованием кортежей - группировка данных
book: tuple[str, str, int]
book = ('Пушкин А.С.', 'Руслан и Людмила', 1818)

# Можно аннотировать и все элементы, независимо от количества
elems: tuple[float, ...]
elems = (2.6, 3.1, 7.4)
elems = (2.6, 3.1, 'one')  # ! swearing

# А вот тут ругаться не будет, потому что int и bool могут быть без
# потерь приведены к float
elems = (2.6, 3.1, 7, True)


""" Словари """
# Аннотируются типы ключа и значения
words: dict[str, int] = {'one': 1, 'two': 2}
words = {'one': 1, 'two': '2'}  # ! swearing

""" Множества """
# Так как изменяемый объект, то так же как для словарей, единый тип для
# всех элементов
persons: set[str] = {'Ann', 'Kate', 'John'}
persons = {'Ann', 2, 'Kate'}  # ! swearing

""" Объявления функций """
# Все тоже самое
def get_positive(digits: list[int]) -> list[int]:
    return list(filter(lambda x: x > 0, digits))

print(get_positive([4, -7, 2, 0, -3]))
print(get_positive([4, -7, 2.6, 0, -3]))  # ! ну понятно :)

# Чтобы не ругалось
def get_positive2(digits: list[int | float]) -> list[int | float]:
    return list(filter(lambda x: x > 0, digits))

print(get_positive2([4, -7, 2.6, 0, -3]))  # все норм

# Аннотации могут быть вложенными
from typing import Optional
def get_positive3(digits: Optional[list[int | float]]) -> list[int | float]:
    return list(filter(lambda x: x > 0, digits)) if digits else []

# Ну и понятно, если вложенность большая, то это тот самый случай, когда
# оправдано использование алиасов (конкретно тут - скорее нет)
Digits_list = list[int | float]
def get_positive4(digits: Optional[Digits_list] = None) -> Digits_list:
    return list(filter(lambda x: x > 0, digits)) if digits else []

print(get_positive4())

""" Аннотация вызываемых объектов """
# Используется тип Callable модуля typing
from typing import Callable

# Формат: Callable[[TypeArg1, TypeArg1, ...], ReturnType]

def get_digits(flt, lst = None):
    return [] if lst is None else list(filter(flt, lst))

# Аннотируем
def get_digits2(flt: Callable[[int], bool], lst: list | None = None) -> list[int]:
    return [] if lst is None else list(filter(flt, lst))

print(get_digits2(lambda x: x % 2 == 0, [1,2,3,4,5,6,7]))

def even(x: int) -> bool:
    return x % 2 == 0

print(get_digits2(even, [1,2,3,4,5,6,7]))

# Ничего не принимает и не возвращает
def empty():
    pass
# Аннотация вызова подобной функции: Callable[[], None]

