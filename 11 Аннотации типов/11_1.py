"""
#
# * 11.1  Аннотация базовыми типами
#
@Links
https://docs.python.org/3/library/typing.html
https://typing.readthedocs.io/en/latest/
"""

# Аннотация переменной. Применяется редко, чаще параметры функций

# ! Все аннотации носят ИНФОРМАЦИОННЫЙ характер и не оказывают влияние
# ! на выполнение программы.

# Без присвоения
k: int
f: float

# С присвоением
s: str = 'string'
l: list = []

k = 'c'  # Нет ошибки, но или IDE или линтер выдадут предупреждение


""" Аннотация параметров функций """

def mul2(x: float):
    return x * 2

# Можно получить аннотации
print(mul2.__annotations__)  # {'x': <class 'float'>}

# Не мешает значениям по умолчанию
def mul2(x: float, y: float = 2):
    return x * y

print(mul2.__annotations__)  # {'x': <class 'float'>, 'y': <class 'float'>}

# Аннотирование возвращаемого типа.
def mul2(x: float, y: float = 2) -> float:
    return x * y

print(mul2.__annotations__)
# {'x': <class 'float'>, 'y': <class 'float'>, 'return': <class 'float'>}

# None - если не ничего не возвращает
def show_x(x: int) -> None:
    print(f'x = {x}')

print(show_x.__annotations__)  # {'x': <class 'int'>, 'return': None}

""" Несколько возможных типов. Union """

from typing import Union, Optional, Any, Final

# Union - объединение нескольких типов

x: Union[int, float]
def mul2(x: Union[int, float], y: Union[int, float] = 2) -> Union[int, float]:
    return x * y

print(mul2.__annotations__)
# {'x': typing.Union[int, float], 'y': typing.Union[int, float], 'return': typing.Union[int, float]}

# Начиная с 3.10 возможна другая запись, равнозначная Union, через
# вертикальную черту |

x: int | float | str  # равнозначна Union(int, float, str)
def mul2(x: int | float, y: int | float = 2) -> int | float:
    return x * y

print(mul2.__annotations__)
# {'x': int | float, 'y': int | float, 'return': int | float}

# Если аннотация громоздка, то можем присвоить её переменной и
# использовать уже переменную. Термин `alias`
# ! Надо использовать с осторожностью, чтобы не загромождать.
# А то вместо удобства, придется искать эти определения


Digits = int | float
def mul2(x: Digits, y: Digits = 2) -> Digits:
    return x * y

print(mul2.__annotations__)
# {'x': int | float, 'y': int | float, 'return': int | float}


""" Тип Optional """
# Позволяет указать только один тип данных, и автоматом добавляет
# тип None

# Следующие выражения полностью равнозначны
Str = Optional[str]
Str = Union[str, None]
Str = str | None

def show_x(x: int, descr: Optional[str] = None) -> None:
    if descr is None:
        descr = 'x = '
    print(f'{descr} {x}')

show_x(5)
show_x(5, 'Это пять: ')
print(show_x.__annotations__)
# {'x': <class 'int'>, 'descr': typing.Optional[str], 'return': None}


""" Тип Any """
# Позволяет аннотировать использование любого типа данных.
# ? Импорт не нужен, есть any, но линтер ругается

def show_x(x: Any, descr: Optional[str] = None) -> None:
    if descr is None:
        descr = 'x = '
    print(f'{descr} {x}')

show_x((1,))
print(show_x.__annotations__)
# {'x': typing.Any, 'descr': typing.Optional[str], 'return': None}


""" Тип Final """
# Служит для отметки констант

MAX_VALUE: Final = 1000
MAX_VALUE = 200  # Будет ругаться, но не во время выполнения конечно
# ! Просто напоминание. Только аннотация только информация