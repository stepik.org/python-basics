"""
#
# * 11.3 Аннотации типов на уровне классов
#
"""
from typing import Any

# Например базовый класс object


x: object
# И что мы не присваивай x, это будет корректно, так как все они
# наследуются от object. Ниже все корректно:
x = 1
x = 'string'
x = False

# Свои классы
class Geom: pass
class Line(Geom): pass


# Это корректно, так как Line является наследником Geom:
g: Geom
g = Line()

# А так - нет:
class NewLine: pass

g = NewLine()  # ! swearing


""" Any и object """
# Разница в следующем:
# ! Тип Any совместим с любым другим типом, а тип object - ни  с каким другим
# 

# Пример:
a: Any = None
b: object = None
s: str

# Тогда:
s = a  # Это корректно
s = b  # ! А это нет

# Наоборот то можно, потому что str мы можем привести к object,
# а вот object к str - нет.
b = s  # корректно

""" Аннотация  """

class NewGeom: pass
class Point2D(NewGeom): pass

# Функция должна создавать объект либо одного, либо другого класса
# Тут cls_obj это ссылка на класс.
# ! Не на объект класса
# Ну и возвращаем обьект
def factory_object(cls_obj):
    return cls_obj()

# Если мы аанотируем так:
def factory_object1(cls_obj: NewGeom) -> NewGeom:
    return cls_obj()
# То будет ошибка, потому что аннотация указывает на ОБЪЕКТ класса NewGeom
# Для того, чтобы указать что аргумент будет ссылаться непосредственно
# на класс, а не на его объекты, существует специальный тип Type в
# модуле typing
from typing import Type
# И тогда
def factory_object2(cls_obj: Type[NewGeom]) -> NewGeom:
    return cls_obj()  # Тогда все корректно

obj1: NewGeom = factory_object2(NewGeom)

# А вот так будет ошибка на уровне аннотации
obj2: Point2D = factory_object2(Point2D)

# Для решения такой проблемы существует специальный тип TypeVar
# модуля typing
from typing import TypeVar

# После чего делается следующее
T = TypeVar('T', bound=NewGeom)

# и тогда допустимым является сам класс NewGeom и любые его дочерние
# классы
def factory_object3(cls_obj: Type[T]) -> T:
    return cls_obj()

obj3: Point2D = factory_object3(Point2D)  # теперь все корректно

# Примеры использования
T1 = TypeVar('T1')  # Любые типы
T2 = TypeVar('T2', int, float)  # Любые из указаных


""" Аннотации внутри классов """
class Point3D(Geom):
    def __init__(self, x: int, y: int, z: int) -> None:
        self.x = x
        self.y = y
        self.z = z
# При такой аннотации - это корректно
xx = Point3D(10,20,2)  # Так корректно, все типы соответствуют
xx.x = 2.1  # Тут предупреждение сработает из-за присваивания self.x = x
# Определение происходит по типу первого присваемого объекта. Чтобы
# работало всегда, надо произвести аннотацию атрибутов заранее
class Point3D_2(Geom):
    # С этой аннотацией предупреждение о несоответствии
    # классов будет всегда
    x: int
    y: int
    z: int
    def __init__(self, x: int, y: int, z: int) -> None:
        self.x = x
        self.y = y
        self.z = z
        
    # Определим функцию copy, которая будет возвращать копию экземпляра
    def copy(self) -> Point3D_2:  # Выдаст предупреждение Point3D_2 не
                                    # определен
        return Point3D_2(self.x, self.y, self.z)
# варианта два
# 1. Заключить в кавычки (устарело). До версии 3.7
    def copy2(self) -> 'Point3D_2':
        return Point3D_2(self.x, self.y, self.z)
# 2. Импортировать annotations из модуля __future__
# from __future__ import annotations
# Тогда кавычки не понадобятся, и все будет нормально работать просто с
# именем класса, т.е. первый вариант: def copy(self) -> Point3D_2:

# ! Важно. Используйте аннотации с умом. Ими можно так замусорить код,
# ! что разбираться в нем будет очень не просто

