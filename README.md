# Добрый, добрый Python

## Обучающий курс от Сергея Балакирева


Курс на [stepik.org](https://stepik.org)

[Ссылка на курс](https://stepik.org/course/100707/syllabus)

[Ссылка на плейлист](https://www.youtube.com/playlist?list=PLA0M1Bcd0w8yWHh2V70bTtbVxJICrnJHd)

На момент написания, 11-й и 12-й раздел в курсе отсутствуют и представляют собой мое повторение и разбор для себя примеров из видео, которые представлены в плейлисте на Youtube.
