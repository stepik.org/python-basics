"""
#
# * 12.1 Конструкция match/case. Первое знакомство
#
"""

'''
Конструкция:
# ? Наверное все-таки не переменная, а выражение
match <переменная>:
    case <шаблон 1>:
        операторы
    ...
    case <шаблон N>:
        операторы
    case _:
        иначе (default)
'''
# Простейший случай
cmd = 'top'
match cmd:
    case 'top':
        print('вверх')
    case 'left':
        print('влево')
    case 'right':
        print('вправо')
    case _:
        print('другое')
# Несколько условий
# Через вертикальную черту |
match cmd:
    case 'top' | 'left' | 'right':
        print('вверх, влево или вправо')
    case _:
        print('другое')

# ! Важно! в case проверяется именно ШАБЛОН

# Использование переменных
match cmd:
    case command:
        print(f'Команда {command}')
# ! Важно переменная будет доступна и во вне блока match
# ! Еще важно. Это аналог _, сработает в любом случае, просто _
# ! используется, если значение переменной не нужно
print(command)

# Проверка типов
# В шаблоне тип со скобками
match cmd:
    case str():
        print('string')
    case int():
        print('integer')
# Проверка типов с созданием переменной 'as'
match cmd:
    # @может быть равнозначная запись, но с 'as' мне кажется лучше
    # case str(command): # равнозначно
    case str() as command:
        print(f'string: {command}')
    case int():
        print('integer')
# ! Важно. В "case str() as command:" сначала идет проверка на тип, и
# ! только потом создается переменная. Если проверка не прошла, то и
# ! переменная создана не будет

# ! Опять важно! Помнить об очередности bool-int. Если int будет
# ! впереди, то до bool дело не дойдет, так как проверка идет по isinstance


# Дополнительные проверки в шаблонах

match cmd:
    case str() as command:
        print(f'string: {command}')
    # Проверим, что не только целое, но и в диапазоне от 1 до 9
    case int() as number if 0 < number < 10:
        print('integer')
# @Терминология
# Этот оператор if получил название guard(защитник)

# Пример
match cmd:
    # Строка длиной меньше 10 символов и первый из них "c"
    case str() as command if len(command) < 10 and command[0] == 'c':
        print(f'C-commnad: {command}')
    # Целое или вещественное в диапазоне от 0 до 9
    case int() | float() as number if 0 <= number <= 0:
        print('digit command')
