#
# * Конструкция match/case с кортежами и списками
#

# Допустим кортеж о книге. Автор, название, цена
cmd = ('Балакиерев', 'Python', 120.45)

# Шаблон по типу, работает так же
match cmd:
    case tuple() as book:
        print(f'кортеж: {book}')
    case _:
        print('Непонятный формат данных')

# Так как кортеж упорядоченый тип, мы можем сделать распаковку данных
# сразу в case
match cmd:
    # Сработает если в кортеже ровно 3 элемента
    case author, title, price:
        print(f'Автор: {author}, Название: {title}, Цена: {price}')
    case _:
        print('Непонятный формат данных')

# Берём только первые три элемента
# Добавим год издания, например
cmd = ('Балакиерев', 'Python', 120.45, 2022)
match cmd:
    case author, title, price, *_:  # После 3-го не используем
        print(f'Автор: {author}, Название: {title}, Цена: {price}')
    case _:
        print('Непонятный формат данных')

# Точно так же работает и со списками. Ничем не отличается
cmd = ['Балакиерев', 'Python', 120.45, 1, 2, 4, 98]
match cmd:
    case author, title, price, *_:  # После 3-го не используем
        print(f'Автор: {author}, Название: {title}, Цена: {price}')
    case _:
        print('Непонятный формат данных')

# Если хотим ограничить длину, можем использовать guard
match cmd:
    case author, title, price, *_ if len(cmd) < 6:
        print(f'Автор: {author}, Название: {title}, Цена: {price}')
    case _:
        print('Непонятный формат данных')

# Для лучшей читаемости можем использовать группирующие скобки
# () или [] - все равно
match cmd:
    # case [author, title, price, *_] if len(cmd) < 6:
    case (author, title, price, *_) if len(cmd) < 6:
        print(f'Автор: {author}, Название: {title}, Цена: {price}')
    case _:
        print('Непонятный формат данных')

# Распаковка с проверкой типа
match cmd:
    case (str() as author, str() as title, float() | int() as price, *_) if len(cmd) < 6:
        print(f'Автор: {author}, Название: {title}, Цена: {price}')
    case _:
        print('Непонятный формат данных')

# Допустим у нас может быть два разных формата данных. Как было
cmd = ('Балакиерев', 'Python', 120.45)
# Или список с id и годом издания
cmd = [1, 'Балакиерев', 'Python', 120.45, 2022]

match cmd:
    case (author, title, price):
        print(f'Книга 1: Автор: {author}, Название: {title}, Цена: {price}')
    case (_, author, title, price, _):
        print(f'Книга 2: Автор: {author}, Название: {title}, Цена: {price}')
    case _:
        print('Непонятный формат данных')

# Но чтобы избежать дублирования кода, мы оба case можем объединить в
# один. () или [] - не важно. Это просто группировка.
match cmd:
    # ! Тут один нюанс - переменный ДОЛЖНЫ называться одинаково и
    # ! количество должно совпадать
    case (author, title, price) | (_, author, title, price, _):
        print(f'Книга : Автор: {author}, Название: {title}, Цена: {price}')
    case _:
        print('Непонятный формат данных')

# Те, если во втором формате нам нужен и год издания, то надо создавать
# отдельный case. (см нюанс)
match cmd:
    case (author, title, price):
        print(f'Книга: {author}, {title}, {price}')
    case (_, author, title, price, year):
        print(f'Книга: {author}, {title}, {price}, {year}')
    case _:
        print('Непонятный формат данных')

# Если мы не хотим обрабатывать кортежи например, то можно их исключить
cmd = ('Балакиерев', 'Python', 120.45)
match cmd:
    case tuple():
        print('Неверный формат данных - кортеж')
    case (author, title, price):
        print(f'Книга: {author}, {title}, {price}')
    case (_, author, title, price, year):
        print(f'Книга: {author}, {title}, {price}, {year}')
    case _:
        print('Непонятный формат данных')

# Все выше рассмотренное относится ко всем Sequtnce Types - list, tuple, range