#
# * Конструкция match/case со словарями и множествами
#

# Словари
# sourcery skip: list-literal
request = {'url': 'http://qqq.ru', 'method': 'GET', 'timeout': 1000}

match request:
    # Как только в шаблоне появляются {}, ожидаются данные 'ключ: значение'
    # В передаваемом словаре есть ключи url и method
    # Наличие или отсутствие других ключей не играет никакой роли
    case {'url': url, 'method': method}:
        print(f'Запрос: url: {url}, method: {method}')
    case _:
        print('Неверный запрос')

# Проверка с типом данных. В любом виде.
match request:
    case {'url': str() as url, 'method': str(method)}:
        print(f'Запрос: url: {url}, method: {method}')
    case _:
        print('Неверный запрос')

# Ограничение по значению
match request:
    # Сработает при совпадении типов и значении timeout равным 1000
    case {'url': str() as url, 'method': str(method), 'timeout': 1000}:
        print(f'Запрос: url: {url}, method: {method}')
    case _:
        print('Неверный запрос')
# Несколько значений
match request:
    # Сработает при совпадении типов и значении timeout равным 1000 или 2000
    case {'url': str() as url, 'method': str(method), 'timeout': 1000 | 2000}:
        print(f'Запрос: url: {url}, method: {method}')
    case _:
        print('Неверный запрос')

# Помним, что можно и комбинировать шаблоны. В данном конкретном случае
# это избыточно. Но в других, при более сложных условиях может пригодится
match request:
    # Сработает при совпадении типов и значении timeout равным 1000 или 2000
    case {'url': str() as url, 'method': str(method), 'timeout': 1000} | {'url': str() as url, 'method': str(method), 'timeout': 2000}:
        print(f'Запрос: url: {url}, method: {method}')
    case _:
        print('Неверный запрос')

# Обрабатываем словари с не более чем 3-мя ключами. Используем guard
match request:
    case {'url': str() as url, 'method': str(method)} if len(request) <= 3:
        print(f'Запрос: url: {url}, method: {method}')
    case _:
        print('Неверный запрос')

# Хотим обработать с ключами url, method и еще не более двух ключей
match request:
    case {'url': str() as url, 'method': str(method), **kwargs} if len(kwargs) <= 2:
        print(f'Запрос: url: {url}, method: {method}')
    case _:
        print('Неверный запрос')

# Или только два наших ключа
match request:
    case {'url': str() as url, 'method': str(method), **kwargs} if not kwargs:
        print(f'Запрос: url: {url}, method: {method}')
    case _:
        print('Неверный запрос')

# Более сложные данные. data - список, если type - list
json_data = {'id': 2, 'type': 'list', 'data': [1, 2, 3], 'access': True, 'date': '01.01.2023'}

match json_data:
    # type - конкретное значение, data - список
    case {'type': 'list', 'data': list() as lst}:
        print(f'JSON-data, type - list: {lst}')
    case _:
        print('неверно')
        
# Еще усложним данные
json_data = {'id': 2,'access': True, 'info': ['01.01.2023', {'login': '123', 'email': 'mail@q.ru'}, True, 1000]}

# Задача. Проверить наличие ключей access и info и из info выделить email
match json_data:
    case {'access': access, 'info': [_, {'email': email}, _, _]}:
        print(f'JSON access: {access}, email: {email}')
    case _:
        print('неверно')
        
# Множества
primary_keys = {1, 2, 3}

match primary_keys:
    # {} использовать нельзя. Будет ожидаться словарь.
    case set() as keys if len(keys) == 3:
        # Тут уже работаем с множеством
        print(f'Primary keys: {keys}')
    case _:
        print('неверно')
