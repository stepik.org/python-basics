# 
# * Конструкция match/case. Примеры и особенности использования
# 

# @Пример 1:
# Обеспечиваем наличие трех обязательных ключей и одного опционального
# Идем от самого частного с к более общему
def connect_db(connect: dict) -> str:
    match connect:
        # 4 обязательных ключа
        case {'server': host, 'login': login, 'password': psw, 'port': port}:
            return f'connection: {host}@{login}.{psw}:{port}'
        # 3 обязательных, 4-й назначаем по дефолту
        case {'server': host, 'login': login, 'password': psw}:
            port = 22
            return f'connection: {host}@{login}.{psw}:{port}'
        case _:
            return 'error connection'

# Но тут можно убрать дублирование кода и оптимизировать
# Просто формируем нужные переменные
def connect_db(connect: dict) -> str:
    match connect:
        case {'server': host, 'login': login, 'password': psw, 'port': port}:
            pass
        case {'server': host, 'login': login, 'password': psw}:
            port = 22
        case _:
            return 'error connection'

    return f'connection: {host}@{login}.{psw}:{port}'


request = {'server': '127.0.0.1', 'login': 'root', 'password': '1234', 'port': 24}

print(connect_db(request))

# @Пример 2:
# Предположим информация о книгах может быть представлена в разных форматах
book_1 = ('Балакирев', 'Python', 2022)
book_2 = ['Балакирев', 'Python ООП', 2022, 125.12]
book_3 = {'author': 'Балакирев', 'title': 'Нейросети', 'year': 2020}
book_4 = {'author': 'Балакирев', 'title': 'Keras+Transflow', 'price': 3420, 'year': 2019}

# Задача. Написать функцию, которая при передаче таких данных формирует
# кортеж в виде (автор, название, год, цена). Если каких-то значений
# нет, вместо них записыватся None

def book_to_tuple(data: tuple | list | dict) -> tuple:
    match data:
        case author, title, year:
            price = None
        # Тут *_ на всякий случай, если список или кортеж имеют больше элементов
        case author, title, year, price, *_:
            pass
        # Это должно быть раньше следующего, как более частный
        case {'author': author, 'title': title, 'year': year, 'price': price}:
            pass
        case {'author': author, 'title': title, 'year': year}:
            price = None
        # Непонятный формат
        case _:
            return None
        
    return author, title, year, price

# Можно немного переделать, избегая дублирования кода
def book_to_tuple(data: tuple | list | dict) -> tuple:
    # price сразу в None, и если присутствует в данных, то обретает значение
    price = None
    match data:
        case author, title, year:
            pass
        case author, title, year, price, *_:
            pass
        case {'author': author, 'title': title, 'year': year, 'price': price}:
            pass
        case {'author': author, 'title': title, 'year': year}:
            pass
        case _:
            return None
        
    return author, title, year, price


print(book_to_tuple(book_1))
print(book_to_tuple(book_2))
print(book_to_tuple(book_3))
print(book_to_tuple(book_4))

# Усложняем задачу. Надо проверить что год издания целочисленый и
# находится в диапазоне, который передается как параметр функции и имеет
# значения по умолчанию от 1800 до 3000
def book_to_tuple(data: tuple | list | dict, min_year: int = 1800, max_year: int = 3000) -> tuple:
    price = None
    match data:
        case author, title, int(year) if min_year < year < max_year:
            pass
        case author, title, int(year), price, *_ if min_year < year < max_year:
            pass
        case {'author': author, 'title': title, 'year': int(year), 'price': price} if min_year < year < max_year:
            pass
        case {'author': author, 'title': title, 'year': int(year)} if min_year < year < max_year:
            pass
        case _:
            return None
        
    return author, title, year, price

book_1 = ('Балакирев', 'Python', 3022)
print(book_to_tuple(book_1))

# Но смотрится не очень аккуратно и guard дублируется. Так что для
# улучшения читаемости, хорошо было бы совместить match и if
def book_to_tuple(data: tuple | list | dict, min_year: int = 1800, max_year: int = 3000) -> tuple:
    price = None
    match data:
        case author, title, int(year):
            pass
        case author, title, int(year), price, *_:
            pass
        case {'author': author, 'title': title, 'year': int(year), 'price': price}:
            pass
        case {'author': author, 'title': title, 'year': int(year)}:
            pass
        case _:
            return None

    return (author, title, year, price) if min_year < year < max_year else None

book_1 = ('Балакирев', 'Python', 3022)
print(book_to_tuple(book_1))


'''
# * Особенности использования констант
'''

# Пример плохого кода
cmd = 10
match cmd:
    case 3:
        print('3')
    case 5:
        print('5')

# 3 и 5 надо бы выделить в константы и прописать уже их в операторе case
# Так вот код ниже работать не будет. Всегда будет срабатывать первый
# case, и переменной CONST_3 будет присвоено значение cmd
# CONST_3 = 3
# CONST_5 = 5
# cmd = 3
# match cmd:
#     case CONST_3:
#         print('3')
#     case CONST_5:
#         print('5')
# Собственно и не выполнится, выдаст ошибку
# SyntaxError: name capture 'CONST_3' makes remaining patterns unreachable

# Есть два решения
# 1-й - проверить на тип и поставить guard
CONST_3 = 3
CONST_5 = 5
cmd = 3
match cmd:
    case int(cmd) as x if x == CONST_3:
        print('3')
    case int(cmd) as x if x == CONST_5:
        print('5')
# Но выглядит громоздко, и в этом случай даже if-elif-else лучше

# 2-й - можно использовать переменные как константы, если перед ними
# стоит точка. Т.е. переменная из класса или модуля. см. my_const.py

# из модуля
import my_const

match cmd:
    case my_const.CONST_3:
        print('3')
    case my_const.CONST_5:
        print('5')


# или из класса. Оформить эти переменные как атрибуты класса и обращаться к ним

class Consts:
    CONST_3 = 3
    CONST_5 = 5

cmd = 5
match cmd:
    case Consts.CONST_3:
        print('3')
    case Consts.CONST_5:
        print('5')

# ! Еще раз, если перед именем переменной в case находится точка, то эту
# ! переменную надо воспринимать как константу Consts.CONST_5