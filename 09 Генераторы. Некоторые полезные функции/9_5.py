#
# * 9.5 Функция zip
#
'''
Подвиг 1. Вводятся два списка целых чисел. Необходимо попарно перебрать
их элементы и перемножить между собой. При реализации программы
используйте функции zip и map. Выведите на экран первые три значения,
используя функцию next. Значения выводятся в строчку через пробел.
(Полагается, что три выходных значения всегда будут присутствовать).
# *Sample Input:
-7 8 11 -1 3
1 2 3 4 5 6 7 8 9 10
# *Sample Output:
-7 16 33
'''


# l1 = list(map(int, input().split()))
# l2 = list(map(int, input().split()))
# z = list(map(lambda x: x[0] * x[1], zip(l1,l2)))
# print(*z[:3])

# Теперь коротенько
# print(*list(
#     map(lambda x: x[0] * x[1],
#         zip(map(int,
#                 input().split()), map(int,
#                                       input().split()))))[:3])

'''
Подвиг 2. Вводится неравномерная таблица целых чисел. С помощью функции
zip выровнить эту таблицу, приведя ее к прямоугольному виду, отбросив
выходящие элементы. Вывести результат на экран в виде такой же таблицы чисел.
P. S. Для считывания списка целиком в программе уже записаны начальные строчки.
# *Sample Input:
1 2 3 4 5 6
3 4 5 6
7 8 9
9 7 5 3 2
# *Sample Output:
1 2 3
3 4 5
7 8 9
9 7 5
'''
# import sys

# # считывание списка из входного потока
# lst_in = list(map(str.strip, sys.stdin.readlines()))

# # здесь продолжайте программу (используйте список строк lst_in)
# for s in zip(*zip(*[list(map(int,s.split())) for s in lst_in])):
#     print(*s)

'''
Подвиг 3. Вводится таблица целых чисел. Необходимо сначала эту таблицу
представить двумерным списком чисел, а затем, с помощью функции zip
выполнить транспонирование этой таблицы (то есть, строки заменить на
соответствующие столбцы). Результат вывести на экран в виде таблицы
чисел (числа в строках следуют через пробел).
P. S. Для считывания списка целиком в программе уже записаны начальные
строчки.
# *Sample Input:
1 2 3 4
5 6 7 8
9 8 7 6
# *Sample Output:
1 5 9
2 6 8
3 7 7
4 8 6
'''
# import sys

# # считывание списка из входного потока
# lst_in = list(map(str.strip, sys.stdin.readlines()))

# # здесь продолжайте программу (используйте список строк lst_in)
# [print(*c) for c in zip(*[map(int, s.split()) for s in lst_in])]

'''
Подвиг 4. Вводится строка из слов, записанных через пробел. Необходимо
на их основе составить прямоугольную таблицу из трех столбцов и N строк
(число строк столько, сколько получится). Лишнее (выходящее) слово - 
отбросить. Реализовать эту программу с использованием функции zip.
Результат отобразить на экране в виде прямоугольной таблицы из слов,
записанных через пробел (в каждой строчке).
# *Sample Input:
Москва Уфа Тула Самара Омск Воронеж Владивосток Лондон Калининград Севастополь
# *Sample Output:
Москва Уфа Тула
Самара Омск Воронеж
Владивосток Лондон Калининград
'''
'''
# ! Нифига не понял, надо разобраться
# ! https://stepik.org/lesson/567075/step/5?unit=561349
res = zip(*[iter(input().split())]*3)
for x in res:
    print(*x)
'''
# [print(*c) for c in zip(*[iter(input().split())]*3)]

'''
Подвиг 5. Вводится строка. Требуется, используя введенную строку,
сформировать N=10 пар кортежей в формате:
(символ, порядковый индекс)
Первый индекс имеет значение 0. Строка может быть короче 10 символов, а
может быть и длиннее. То есть, число пар может быть 10 и менее.
Используя функцию zip сформируйте указанные кортежи и сохраните в список
с именем lst.
Программа ничего не должна отображать на экране, только формировать список из кортежей.
# *Sample Input:
Python дай мне силы пройти этот курс до конца!
# *Sample Output:
True
'''
# считывание строки в переменную s (эту переменную в программе не менять)
s = input()

# здесь продолжайте программу
lst = list(zip(s,range(10)))

